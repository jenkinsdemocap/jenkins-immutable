import jenkins.*
import hudson.*
import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.common.*
import com.cloudbees.plugins.credentials.domains.*
import com.cloudbees.jenkins.plugins.sshcredentials.impl.*
import hudson.plugins.sshslaves.*;
import hudson.model.*
import jenkins.model.*
import hudson.security.*
import org.jenkinsci.plugins.workflow.libs.*
import hudson.scm.SCM;
import hudson.plugins.git.*;

def j = Jenkins.getInstance()

def e = { filepath ->
  def env = System.getenv()
  evaluate(new File(env["JENKINS_HOME"] + '/init.groovy.d/' + filepath))
}

def set_user = e("./../init.groovy.mixins/SetUser.groovy")
def user_credential = e("./../init.groovy.mixins/UserCredentials.groovy")
def ssh_credential = e("./../init.groovy.mixins/SshCredentials.groovy")

if (!j.installState.isSetupComplete()) {
  println '--> Neutering SetupWizard'
  InstallState.INITIAL_SETUP_COMPLETED.initializeState()
}

def hudsonRealm = new HudsonPrivateSecurityRealm(false)
hudsonRealm.createAccount("alan","fred")
j.setSecurityRealm(hudsonRealm)

def strategy = new hudson.security.FullControlOnceLoggedInAuthorizationStrategy()
strategy.setAllowAnonymousRead(false)
j.setAuthorizationStrategy(strategy)

set_user(username='alan', fullname='alan', email='me@alanhollis.com')

user_credential(
    instance = j,
    username = 'alan',
    password = 'fred',
	id       = 'alan',
	description = 'alan'
)

String bitbucket_pass = new File('/var/run/secrets/bitbucket_user_pass').text


user_credential(
	instance = j,
	username = 'jenkins-demo-cap',
	password = bitbucket_pass.trim(),
	id       = 'jenkins-demo-cap',
	description = 'jenkins-demo-cap',
)

ssh_credential(
	instance = j,
	id = "jenkins-agent",
	username = "jenkins",
	description = "jenkins",
)

j.getDescriptor("jenkins.CLI").get().setEnabled(false)

def desc = j.getDescriptor("org.jenkinsci.plugins.workflow.libs.GlobalLibraries")
SCM scm = new GitSCM("https://bitbucket.org/jenkinsdemocap/jenkins-shared-lib.git")
SCMRetriever retriever = new SCMRetriever(scm)
def name = "shared-lib"
LibraryConfiguration libconfig = new LibraryConfiguration(name, retriever)
libconfig.setImplicit(true)
libconfig.setDefaultVersion("master")

desc.get().setLibraries([libconfig])


j.save()
